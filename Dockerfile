FROM keymetrics/pm2:16-alpine

USER root
ENV APP_HOME /usr/share/pm2_mem


RUN mkdir -p "${APP_HOME}"

RUN addgroup -S pm2_mem && adduser -S pm2_mem -G pm2_mem -h "${APP_HOME}"
RUN chown pm2_mem:pm2_mem "${APP_HOME}"

WORKDIR "${APP_HOME}"

COPY . .
RUN rm -rf node_modules
ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install --production

USER pm2_mem

# Show current folder structure in logs
RUN ls -al -R

CMD [ "pm2-runtime", "start", "pm2.json" ]