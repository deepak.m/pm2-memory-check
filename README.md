Run locally

```
node --max-old-space-size=256 index.js
```

Docker build 

```
docker build -t pm2_mem_check .
```

Docker run

```
docker run --rm --name pm2 pm2_mem_check
```

Pm2 monit while running     
```
docker exec -it pm2 pm2 monit
```
